import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd._
import scala.tools.nsc.io.File

object GenomeSpark {
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("Movie Genome").getOrCreate()
        val (genes, movies) = loadData(spark)
        run(genes, movies, 1140)
        //knn.map(movies.lookup(_).head) foreach println
        spark.stop()
    }

    def run(genes: RDD[(Int, (Int, Double))],
            movies: RDD[(Int, String)], atmost: Int) {
        val genByMovie = genes.groupByKey().cache()
        val preprocessed = getProcessed()
        val failed = getFailed()
        println("Preprocessed: " + preprocessed.mkString(","))
        var i = 0
        for(m <- movies.collect
            if (!preprocessed.contains(m._1) && !failed.contains(m._1))) {
            val knn = knearest(m._1, 100, genByMovie)
            if (knn.length > 0) {
                logAppend(m._1, knn)
                i += 1
            } else {
                logFailed(m._1)
            }
            if (i > atmost) return
        }
    }

    def knearest(movieId: Int, k: Int,
                 genome: RDD[(Int, Iterable[(Int, Double)])]) = {
        def summation(p: Iterable[(Int, Double)],
                      q: Iterable[(Int, Double)]) = {
            val squareOfDiff =
                for((tag, relevance1) <- p;
                    (`tag`, relevance2) <- q;
                    diff = relevance2 - relevance1
                ) yield diff * diff
            squareOfDiff.sum
        }

        try {
            val p = genome.lookup(movieId).head
            val distances =
                    for (movie <- genome;
                         q = movie._2;
                         distance = math.sqrt(summation(p, q))
                         if (movie._1 != movieId)
                    ) yield (movie._1, distance)
            distances.sortBy(_._2).take(k).map(_._1)
        } catch {
            case _ : Throwable => Array[Int]()
        }
    }


    val filename = "knnGenome-ml-25m.txt"
    def getProcessed() = {
        File(filename).createFile().lines().map(l => l.split(",")(0).toInt).toSet
    }

    def getFailed() = {
        File("failed-" + filename).createFile().lines().map(l => l.toInt).toSet
    }

    def logFailed(id: Int) = {
        File("failed-" + filename).appendAll(id.toString + "\n")
    }
    def logAppend(id: Int, knn: Array[Int]) = {
        File(filename).appendAll(id.toString + "," + knn.mkString(",") + "\n")
    }

    def loadData(spark: SparkSession): 
                             (RDD[(Int, (Int, Double))], RDD[(Int, String)]) = {
        import spark.implicits._
        val movieLensHomeDir = "/cs449/movielens/ml-25m"
        val genome = spark.read.options(Map("header" -> "true"))
                        .csv(movieLensHomeDir + "/genome-scores.csv").rdd
                        .map(r => (r.getString(0).toInt,
                                   (r.getString(1).toInt, r.getString(2).toDouble)))
        val movies =  spark.read.format("csv").option("header", "true")
                           .load(movieLensHomeDir + "/movies.csv").rdd
                           .map(r => (r.getString(0).toInt, r.getString(1)))
        (genome, movies.cache())
    }
}
